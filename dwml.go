package main

import (
	"encoding/xml"
	"io/ioutil"
	"net/http"
)

type Temperature struct {
	XMLName xml.Name `xml:"temperature"`
	Name    string   `xml:"name"`
	Value   int      `xml:"value"`
}

type Parameters struct {
	XMLName      xml.Name      `xml:"parameters"`
	Temperatures []Temperature `xml:"temperature"`
}

type Data struct {
	XMLName    xml.Name   `xml:"data"`
	Parameters Parameters `xml:"parameters"`
}

type Dwml struct {
	XMLName      xml.Name `xml:"dwml"`
	LatLonList   string   `xml:"latLonList"`
	CityNameList string   `xml:"cityNameList"`
	Data         Data     `xml:"data"`
}

func NewDwml(url string) (dwml *Dwml, err error) {
	client := &http.Client{}

	response, err := client.Get(url)

	if err != nil {
		return nil, err
	}

	defer response.Body.Close()

	body, err := ioutil.ReadAll(response.Body)

	if err != nil {
		return nil, err
	}

	dwml = new(Dwml)

	err = xml.Unmarshal(body, dwml)

	if err != nil {
		return nil, err
	}

	return dwml, nil
}
