package main

import (
	"html/template"
	"net/http"
	"strings"
)

const (
	PORT = "6969"
)

type Error struct {
	message string
}

func (err *Error) Error() string {
	return err.message
}

type Temperatures struct {
	Low  int
	High int
}

type Page struct {
	Temperatures Temperatures
	CityMap      map[string]string
}

func GetCityMap() (newMap *map[string]string, err error) {
	dwml, err := NewDwml("https://graphical.weather.gov/xml/sample_products/browser_interface/ndfdXMLclient.php?listCitiesLevel=12")

	if err != nil {
		return nil, err
	}

	latLongs := strings.Split(dwml.LatLonList, " ")
	cities := strings.Split(dwml.CityNameList, "|")

	cityMap := make(map[string]string)

	for i, city := range cities {
		cityMap[city] = latLongs[i]
	}

	return &cityMap, nil
}

func GetLatLongTemperatures(latLong string) (temperatures *Temperatures, err error) {
	temperatures = new(Temperatures)

	dwml, err := NewDwml("https://graphical.weather.gov/xml/sample_products/browser_interface/ndfdBrowserClientByDay.php?listLatLon=" + latLong + "&format=24+hourly&numDays=1")

	if err != nil {
		return nil, err
	}

	temperatures.High = dwml.Data.Parameters.Temperatures[0].Value
	temperatures.Low = dwml.Data.Parameters.Temperatures[1].Value

	return temperatures, nil
}

func main() {
	page := Page{}

	cityMap, err := GetCityMap()

	page.CityMap = *cityMap

	if err != nil {
		panic(err)
	}

	http.HandleFunc("/", func(writer http.ResponseWriter, request *http.Request) {
		index, err := template.ParseFiles("index.html")

		if err != nil {
			panic(err)
		}

		err = index.Execute(writer, page)
	})

	http.HandleFunc("/temperature", func(writer http.ResponseWriter, request *http.Request) {
		latLong := request.URL.Query().Get("latLong")

		temperatures, err := GetLatLongTemperatures(latLong)

		if err != nil {
			panic(err)
		}

		page.Temperatures = *temperatures

		index, err := template.ParseFiles("index.html")

		if err != nil {
			panic(err)
		}

		err = index.Execute(writer, page)
	})

	http.HandleFunc("/bootstrap.min.css", func(writer http.ResponseWriter, request *http.Request) {
		http.ServeFile(writer, request, "bootstrap.min.css")
	})

	err = http.ListenAndServe(":" + PORT, nil)

	if err != nil {
		panic(err)
	}
}
